# -*- coding: utf-8 -*-
from itertools import islice
import oss2
from oss2.credentials import EnvironmentVariableCredentialsProvider
# 从环境变量中获取访问凭证。运行本代码示例之前，请确保已设置环境变量OSS_ACCESS_KEY_ID和OSS_ACCESS_KEY_SECRET。
auth = oss2.ProviderAuth(EnvironmentVariableCredentialsProvider())
# 填写Bucket所在地域对应的Endpoint。以华东1（杭州）为例，Endpoint填写为https://oss-cn-hangzhou.aliyuncs.com。
# yourBucketName填写存储空间名称。
bucket = oss2.Bucket(auth, 'https://oss-cn-shanghai.aliyuncs.com', 'sap-cp-e766eb13-e58f-40bc-9dcd-c96f23eb9773-zrs')

# 上传文件到OSS。
# yourObjectName由包含文件后缀，不包含Bucket名称组成的Object完整路径，例如abc/efg/123.jpg。
# yourLocalFile由本地文件路径加文件名包括后缀组成，例如/users/local/myfile.txt。
# res = bucket.put_object_from_file('avatars/avatar_memoji.jpg', '/Users/i562752/Downloads/avatar_memoji.jpg')
# print(res)

# oss2.ObjectIterator用于遍历文件。
for b in islice(oss2.ObjectIterator(bucket), 10):
    print(b)