package com.chat.message.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.chat.message.model.BusinessMessageEntity;
import com.chat.message.model.JobExecutionStatus;
import com.chat.message.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class NativeSQL {

    Logger logger = LoggerFactory.getLogger(NativeSQL.class);
    private Connection connection = null;
    @Autowired Database db;

    public void createConnection() {

        long startTime = System.currentTimeMillis();

        System.out.println("Java version: " + com.sap.db.jdbc.Driver.getJavaVersion());
        System.out.println("Minimum supported Java version and SAP driver version number: "
                + com.sap.db.jdbc.Driver.getVersionInfo());

        try {
            connection = DriverManager.getConnection(db.getUrl(), db.getUsername(), db.getPassword());

            if (connection != null) {

                System.out.println("Connection to HANA successful!");
            }

            long endTime = System.currentTimeMillis();
            long executeTime = endTime - startTime;

            System.out.println("HANA Connection Time:"  + executeTime);

        } catch (SQLException e) {
            logger.error("Connection Failed:");
            logger.error(e.toString());
            return;
        }
    }

    public void insertJob(JobExecutionStatus job) {
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();

                PreparedStatement pstmt = connection.prepareStatement(
                        "INSERT INTO \"ECM_JOB_EXECUTION_STATUS\" (\"JOB_ID\", \"JOB_NAME\", \"RESULT\", \"START_TIME\", \"STATUS\") VALUES (?, ?, ?, ?, ?)");
                pstmt.setNString(1, job.getJobId());
                pstmt.setNString(2, job.getJobName());
                pstmt.setNString(3, job.getResult());
                pstmt.setLong(4, job.getStartTime());
                pstmt.setNString(5, job.getStatus());

                pstmt.executeUpdate();

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Insert Job:" + job.getJobId() + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Insert failed!");
                logger.error(e.toString());
            }
        }
    }

    public void updateJob(JobExecutionStatus job) {
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();

                PreparedStatement pstmt = connection.prepareStatement(
                        "UPDATE \"ECM_JOB_EXECUTION_STATUS\" SET \"RESULT\" = ?, \"STATUS\" = ? WHERE (\"JOB_ID\" = ?)");
                pstmt.setNString(1, job.getResult());
                pstmt.setNString(2, job.getStatus());
                pstmt.setNString(3, job.getJobId());
                pstmt.executeUpdate();

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Update Job:" + job.getJobId() + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Update failed!");
                logger.error(e.toString());
            }
        }
    }

    public void getJob(String jobId) {
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();

                PreparedStatement pstmt = connection.prepareStatement(
                        "SELECT \"JOB_ID\", \"JOB_NAME\", \"RESULT\", \"START_TIME\", \"STATUS\" FROM \"ECM_JOB_EXECUTION_STATUS\" WHERE (\"JOB_ID\" = ?)");
                pstmt.setNString(1, jobId);

                ResultSet rs = pstmt.executeQuery();

                if (rs != null && rs.next()) {
                    String job_id = rs.getString("JOB_ID");
                    String job_name = rs.getString("JOB_Name");
                    long start_time = rs.getLong("START_TIME");
                    String status = rs.getString("STATUS");
                    String result = rs.getString("RESULT");

                    System.out.println(job_id + "-" + job_name + "-" + start_time + "-" + status + "-" + result);
                }

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Query Job:" + jobId + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Query failed!");
                logger.error(e.toString());
            }
        }
    }

    public void insertUser(User user) {
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();

                PreparedStatement pstmt = connection.prepareStatement(
                        "INSERT INTO \"USER\" (\"ID\", \"NAME\", \"AVATAR\", \"OPENID\") VALUES (?, ?, ?, ?)");
                pstmt.setNString(1, user.getId().toString());
                pstmt.setNString(2, user.getName());
                pstmt.setNString(3, user.getAvatar());
                pstmt.setNString(4, user.getOpenId());

                pstmt.executeUpdate();

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Insert User:" + user.getId() + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Insert User failed!");
                logger.error(e.toString());
            }
        }
    }

    public void updateUser(User user) {
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();

                PreparedStatement pstmt = connection.prepareStatement(
                        "UPDATE \"USER\" SET \"NAME\" = ? WHERE (\"OPENID\" = ?)");
                pstmt.setNString(1, user.getName());
                pstmt.setNString(2, user.getOpenId());
                pstmt.executeUpdate();

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Update User:" + user.getId() + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Update User failed!");
                logger.error(e.toString());
            }
        }
    }

    public User getUser(UUID userId) {
        User user = new User();
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();

                PreparedStatement pstmt = connection.prepareStatement(
                        "SELECT \"ID\", \"NAME\", \"AVATAR\", \"OPENID\" FROM \"USER\" WHERE (\"ID\" = ?)");
                pstmt.setString(1, userId.toString());

                ResultSet rs = pstmt.executeQuery();

                if (rs != null && rs.next()) {
                    user.setId(UUID.fromString(rs.getString("ID")));
                    user.setName(rs.getString("NAME"));
                    user.setAvatar(rs.getString("AVATAR"));
                    user.setOpenId(rs.getString("OPENID"));
                }

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Query Job:" + userId + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Query User failed!");
                logger.error(e.toString());
            }
        }
        return user;
    }

    // public List<User> getUserList() {
    //     User user = new User();
    //     List<User> userList = new ArrayList<User>();
    //     if (connection != null) {
    //         try {
    //             long startTime = System.currentTimeMillis();

    //             PreparedStatement pstmt = connection.prepareStatement(
    //                     "SELECT \"ID\", \"NAME\", \"AVATAR\", \"OPENID\" FROM \"USER\" WHERE (\"ID\" = ?)");

    //             ResultSet rs = pstmt.executeQuery();

    //             if (rs != null && rs.next()) {
    //                 user.setId(UUID.fromString(rs.getString("ID")));
    //                 user.setName(rs.getString("NAME"));
    //                 user.setAvatar(rs.getString("AVATAR"));
    //                 user.setOpenId(rs.getString("OPENID"));
    //             }

    //             long endTime = System.currentTimeMillis();
    //             long executeTime = endTime - startTime;

    //             System.out.println("Query Job:" + userId + ":" + executeTime);

    //         } catch (SQLException e) {
    //             logger.error("Query User failed!");
    //             logger.error(e.toString());
    //         }
    //     }
    //     return user;
    // }
    
    public List<User> getUserListWithoutCurrentUser(String openId) {
        List<User> userList = new ArrayList<User>();
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();

                PreparedStatement pstmt = connection.prepareStatement(
                        "select ID, NAME, AVATAR, OPENID from USER where OPENID != ?");
                pstmt.setString(1, openId.toString());

                ResultSet rs = pstmt.executeQuery();

                System.out.println(rs);

                while (rs.next()) {
                    User user = new User();
                    user.setId(UUID.fromString(rs.getString("ID")));
                    user.setName(rs.getString("NAME"));
                    user.setAvatar(rs.getString("AVATAR"));
                    user.setOpenId(rs.getString("OPENID"));
                    userList.add(user);
                }

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Query Job:" + openId + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Query User failed!");
                logger.error(e.toString());
            }
        }
        return userList;
    }

    public List<BusinessMessageEntity> getMessageHistory(String myMemberId, String youMemberId, int pageNo, int pageSize) {
        List<BusinessMessageEntity> messageList = new ArrayList<BusinessMessageEntity>();
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();
                // select * from "message" where "del_flag" = 0 and (("sender"='16024575-8513-48a6-9f70-3c1c150f4831' and "receiver"='550e8400-e29b-41d4-a716-446655440002') or "sender"='550e8400-e29b-41d4-a716-446655440002' and "receiver"='16024575-8513-48a6-9f70-3c1c150f4831') order by "created_time";
                PreparedStatement pstmt = connection.prepareStatement(
                        "select * from \"message\" where \"del_flag\" = 0 and ( \"sender\"=? and \"receiver\"=? or \"sender\"=? and \"receiver\"=? ) order by \"created_time\";");
                pstmt.setString(1, myMemberId.toString());
                pstmt.setString(2, youMemberId.toString());
                pstmt.setString(3, youMemberId.toString());
                pstmt.setString(4, myMemberId.toString());

                ResultSet rs = pstmt.executeQuery();

                System.out.println(rs);

                while (rs.next()) {
                    BusinessMessageEntity message = new BusinessMessageEntity();
                    message.setType(rs.getInt("type"));
                    message.setContent(rs.getString("content"));
                    message.setContentType(rs.getInt("content_type"));
                    message.setIsRead(rs.getString("is_read"));
                    message.setSender(rs.getString("sender"));
                    message.setReceiver(rs.getString("receiver"));
                    message.setRequestId(rs.getString("request_id"));
                    message.setIsLast(rs.getBoolean("is_last"));
                    message.setId(rs.getLong("id"));
                    message.setCreatedBy(rs.getString("created_by"));
                    message.setCreatedTime(rs.getDate("created_time"));
                    message.setUpdatedBy(rs.getString("updated_by"));
                    message.setUpdatedTime(rs.getDate("updated_time"));
                    message.setDelFlag(rs.getString("del_flag"));
                    message.setStatus(rs.getString("status"));
                    message.setRemarks(rs.getString("remarks"));
                    messageList.add(message);
                }

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Query Job:" + myMemberId + "/" + youMemberId + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Query User failed!");
                logger.error(e.toString());
            }
        }
        return messageList;
    }

    public User getUserByOpenId(String openId) {
        User user = new User();
        if (connection != null) {
            try {
                long startTime = System.currentTimeMillis();

                PreparedStatement pstmt = connection.prepareStatement(
                        "SELECT \"ID\", \"NAME\", \"AVATAR\", \"OPENID\" FROM \"USER\" WHERE (\"OPENID\" = ?)");
                pstmt.setString(1, openId);

                ResultSet rs = pstmt.executeQuery();

                if (rs != null && rs.next()) {
                    user.setId(UUID.fromString(rs.getString("ID")));
                    user.setName(rs.getString("NAME"));
                    user.setAvatar(rs.getString("AVATAR"));
                    user.setOpenId(rs.getString("OPENID"));
                }

                long endTime = System.currentTimeMillis();
                long executeTime = endTime - startTime;

                System.out.println("Query Job:" + openId + ":" + executeTime);

            } catch (SQLException e) {
                logger.error("Query User failed!");
                logger.error(e.toString());
            }
        }
        return user;
    }

    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
