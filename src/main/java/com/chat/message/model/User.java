package com.chat.message.model;

import java.util.UUID;

import javax.persistence.*;
import lombok.Data;

/**
 * @author lisw
 * @program message
 * @description 用户信息表
 * @createDate 2021-08-18 17:02:41
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
@Data
@Entity
@Table(name = "\"USER\"")
public class User {

    @Id
    @Column(name = "\"ID\"", length = 36)
    private UUID id;
    @Column(name = "\"NAME\"", length = 100)
    private String name;
    @Column(name = "\"AVATAR\"", length = 120)
    private String avatar;
    @Column(name = "\"OPENID\"", length = 28)
    private String openId;
}
