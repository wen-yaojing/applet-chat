package com.chat.message.model;

// import com.baomidou.mybatisplus.annotation.FieldFill;
// import com.baomidou.mybatisplus.annotation.TableField;
// import com.baomidou.mybatisplus.annotation.TableId;
// import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author lisw
 * @program message
 * @description
 * @createDate 2021-08-18 17:03:58
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
@Data
// @TableName("message")
@Entity
@Table(name = "\"message\"")
public class BusinessMessageEntity {
    @Id
    @Column(name = "\"serialVersionUID\"", length = 120)
    private static final long serialVersionUID = 1L;
    /**
     * 是否实现时间
     * 0：否 1：是
     */
    @Id
    @Column(name = "\"type\"", length = 120)
    private Integer type;
    /**
     * 消息内容
     */
    @Id
    @Column(name = "\"content\"", length = 120)
    private String content;
    /**
     * 内容类型 0文字1图片2视频 3礼物
     */
    @Id
    @Column(name = "\"contentType\"", length = 120)
    private Integer contentType;
    /**
     * 是否已读
     */
    @Id
    @Column(name = "\"isRead\"", length = 120)
    private String isRead;
    /**
     * 发送人
     */
    @Id
    @Column(name = "\"sender\"", length = 120)
    private String sender;
    /**
     * 接收人
     */
    @Id
    @Column(name = "\"receiver\"", length = 120)
    private String receiver;

    /**
     * 发送消息请求ID
     */
    @Id
    @Column(name = "\"requestId\"", length = 120)
    private String requestId;

    @Id
    @Column(name = "\"isLast\"", length = 120)
    private Boolean isLast;

    /**
     * 主键ID
     */
    // @TableId
    @Id
    @Column(name = "\"id\"", length = 120)
    private Long id;
    /**
     * 创建人
     */
    // @TableField(value = "created_by",fill = FieldFill.INSERT)
    @Id
    @Column(name = "\"createdBy\"", length = 120)
    private String createdBy;
    /**
     * 创建时间
     */
    // @TableField(value = "created_time",fill = FieldFill.INSERT)
    @Id
    @Column(name = "\"createdTime\"", length = 120)
    private Date createdTime;
    /**
     * 更新人
     */
    // @TableField(value = "updated_by",fill = FieldFill.INSERT_UPDATE)
    @Id
    @Column(name = "\"updatedBy\"", length = 120)
    private String updatedBy;
    /**
     * 更新时间
     */
    // @TableField(value = "updated_time",fill = FieldFill.INSERT_UPDATE)
    @Id
    @Column(name = "\"updatedTime\"", length = 120)
    private Date updatedTime;
    /**
     * 是否删除
     */
    // @TableField(value = "del_flag",fill = FieldFill.INSERT)
    @Id
    @Column(name = "\"delFlag\"", length = 120)
    private String delFlag;
    /**
     * 状态
     */
    @Id
    @Column(name = "\"status\"", length = 120)
    private String status;
    /**
     * 备注
     */
    @Id
    @Column(name = "\"remarks\"", length = 120)
    private String remarks;
}
