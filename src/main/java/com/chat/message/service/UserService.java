package com.chat.message.service;

import com.chat.message.model.User;

import java.util.List;
import java.util.UUID;

public interface UserService {
  public User getOne(UUID userId);
  public User getOne(String openId);
  public void setOne(User user);
  public List<User> listWithoutCurrent(String openId);
}
