package com.chat.message.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chat.message.dao.MessageDao;
import com.chat.message.dao.NativeSQL;
import com.chat.message.dao.UserDao;
import com.chat.message.model.BusinessMessageEntity;
import com.chat.message.model.User;
import com.chat.message.service.MessageService;
import com.chat.message.service.UserService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author lisw
 * @program message
 * @description
 * @createDate 2021-08-18 17:10:38
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
@Service
@Slf4j
public class MessageServiceImpl extends ServiceImpl<MessageDao, BusinessMessageEntity> implements MessageService {

    @Autowired
    NativeSQL nativeSQL;

    // @Before("execution(* com.chat.message.service.UserServiceImpl.*(..))") // TODO
    public void beforeEach() {
        nativeSQL.createConnection();
    }

    // @After("execution(* com.chat.message.service.UserServiceImpl.*(..))") // TODO
    public void afterEach() {
        nativeSQL.closeConnection();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int sendMessage(BusinessMessageEntity businessMessageEntity) {
        //将此二人以往的最后聊天消息，置为不是最后的消息

        // sender = businessMessageEntity.getSender();


        // beforeEach();
        // if (user.getOpenId() != null && nativeSQL.getUserByOpenId(user.getOpenId()) != null)
        //     nativeSQL.updateUser(user);
        // else {
        //     UUID uuid = UUID.randomUUID();
        //     user.setId(uuid);
        //     nativeSQL.insertUser(user);
        // }
        // afterEach();

        // UpdateWrapper<BusinessMessageEntity> updateWrapper = new UpdateWrapper();
        // updateWrapper.set("is_last",0);
        // updateWrapper.eq("del_flag", 0);
        // updateWrapper.and(wrapper -> wrapper.and(item -> item.eq("sender",businessMessageEntity.getSender()).eq("receiver",businessMessageEntity.getReceiver()))
        //         .or(item -> item.eq("receiver",businessMessageEntity.getSender()).eq("sender",businessMessageEntity.getReceiver()))
        // );
        // this.update(updateWrapper);
        // // 保存消息
        // businessMessageEntity.setIsLast(true);
        // businessMessageEntity.setDelFlag("0");
        // this.save(businessMessageEntity);
        // return 1;


        return 1;

    }

    public List<BusinessMessageEntity> getMessageHistory(String myMemberId, String youMemberId, int pageNo, int pageSize) {
        beforeEach();
        // User user =  nativeSQL.getUserByOpenId(openId);
        List<BusinessMessageEntity> messageList = nativeSQL.getMessageHistory(myMemberId, youMemberId, pageNo, pageSize);
        // userList.add(user);
        afterEach();
        if (myMemberId == null || youMemberId == null)
            return null;
        else
            return messageList;
    }
}
