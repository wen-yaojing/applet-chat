package com.chat.message.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chat.message.dao.NativeSQL;
import com.chat.message.dao.UserDao;
import com.chat.message.model.User;
import com.chat.message.service.UserService;
import lombok.extern.slf4j.Slf4j;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author lisw
 * @program message
 * @description
 * @createDate 2021-08-18 17:10:38
 * @slogan 长风破浪会有时，直挂云帆济沧海。
 **/
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {
    @Autowired
    NativeSQL nativeSQL;

    // @Before("execution(* com.chat.message.service.UserServiceImpl.*(..))") // TODO
    public void beforeEach() {
        nativeSQL.createConnection();
    }

    // @After("execution(* com.chat.message.service.UserServiceImpl.*(..))") // TODO
    public void afterEach() {
        nativeSQL.closeConnection();
    }

    public User getOne(UUID userId) {
        beforeEach();
        User user = nativeSQL.getUser(userId);
        afterEach();
        if (user.getId() == null)
            return null;
        else
            return user;
    }

    public User getOne(String openId) {
        beforeEach();
        User user =  nativeSQL.getUserByOpenId(openId);
        afterEach();
        if (user.getId() == null)
            return null;
        else
            return user;
    }

    public void setOne(User user) {
        beforeEach();
        if (user.getOpenId() != null && nativeSQL.getUserByOpenId(user.getOpenId()) != null)
            nativeSQL.updateUser(user);
        else {
            UUID uuid = UUID.randomUUID();
            user.setId(uuid);
            nativeSQL.insertUser(user);
        }
        afterEach();
    }

    public List<User> listWithoutCurrent(String openId) {
        beforeEach();
        // User user =  nativeSQL.getUserByOpenId(openId);
        List<User> userList = nativeSQL.getUserListWithoutCurrentUser(openId);
        // userList.add(user);
        afterEach();
        if (openId == null)
            return null;
        else
            return userList;
    }
}
